/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package streetview.api;

import java.util.Comparator;

/**
 *
 * @author Jeff
 */
public class LocationComparator implements Comparator<Location> {
    private final Location origin;
    public LocationComparator(Location origin) {
        this.origin = origin;
    }
    
    @Override
    public int compare(Location o1, Location o2) {
        StreetView s = new StreetView();
        double d1 = s.distance(origin, o1);
        double d2 = s.distance(origin, o2);
        return Double.compare(d1, d2);
    }
    
}
