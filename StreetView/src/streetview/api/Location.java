/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package streetview.api;

/**
 *
 * @author Jeff
 */
public class Location {
    public double lat = 0.0;
    public double lng = 0.0;
    public double bearing = 0.0;
    
    public Location() {
        
    }
    
    public Location(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }
    
    @Override
    public String toString() {
        return lat + "," + lng;
    }
    
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Location)) {
            return false;
        }
        Location l2 = (Location)o;
        return this.lat == l2.lat && this.lng == l2.lng;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + (int) (Double.doubleToLongBits(this.lat) ^ (Double.doubleToLongBits(this.lat) >>> 32));
        hash = 89 * hash + (int) (Double.doubleToLongBits(this.lng) ^ (Double.doubleToLongBits(this.lng) >>> 32));
        return hash;
    }
}
