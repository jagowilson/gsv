/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package streetview.api;

import java.awt.Image;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import net.sf.geographiclib.Geodesic;
import net.sf.geographiclib.GeodesicData;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import sun.misc.IOUtils;

/**
 *
 * @author Jeff
 */
public class StreetView {

    private static final String API_KEY = "AIzaSyBwv6KcUySUpHdAYNeZCwPy30Xpr7hm8Bk";

    public Location addressToLocation(String address) {
        try {
            String request = "https://maps.googleapis.com/maps/api/geocode/json?"
                    //+ "key=" + API_KEY + "&"
                    + "sensor=false" + "&"
                    + "address=" + URLEncoder.encode(address, "UTF-8");
            JSONObject parse = getJson(request);
            JSONArray results = (JSONArray) parse.get("results");
            JSONObject resultMap = (JSONObject) results.get(0);
            JSONObject geometry = (JSONObject) resultMap.get("geometry");
            JSONObject location = (JSONObject) geometry.get("location");
            Object lat = location.get("lat");
            Object lng = location.get("lng");
            Location l = new Location();
            l.lat = (Double) lat;
            l.lng = (Double) lng;
            return l;
        } catch (Exception e) {
        }

        return null;
    }

    public Route getDirections(Location from, Location to) {
        String request;
        Route r = new Route();
        request = "https://maps.googleapis.com/maps/api/directions/json?"
                + "origin=" + from + "&destination=" + to + "&sensor=false"
                + "&alternatives=true";

        try {
            String json = this.getHttpResponse(request);
            JSONParser p = new JSONParser();
            JSONObject jsonObject = (JSONObject) p.parse(json);
            JSONArray routes = (JSONArray) jsonObject.get("routes");
            JSONObject route = (JSONObject) routes.get(1);
            JSONObject overview_poly = (JSONObject) route.get("overview_polyline");
            JSONArray legs = (JSONArray) route.get("legs");
            JSONObject leg0 = (JSONObject) legs.get(0);
            JSONArray steps = (JSONArray) leg0.get("steps");
            for (int i = 0; i < steps.size(); i++) {
                RouteStep s = new RouteStep();
                JSONObject step = (JSONObject) steps.get(i);
                JSONObject polyline = (JSONObject) step.get("polyline");
                s.direction = (String) step.get("html_instructions");
                JSONObject distance = (JSONObject) step.get("distance");
                s.distance = (long) distance.get("value");
                String points = (String) polyline.get("points");
                s.points.addAll(PolylineDecoder.decodePoly(points));
                r.route.add(s);
            }
        } catch (Exception ex) {
            Logger.getLogger(StreetView.class.getName()).log(Level.SEVERE, null, ex);
        }
        return r;
    }

    private JSONObject getJson(String urlString) throws MalformedURLException, IOException, ParseException {
        JSONParser p = new JSONParser();
        String json = this.getHttpResponse(urlString);
        Object o = p.parse(json);
        JSONObject jsonObject = (JSONObject) o;
        return jsonObject;
    }

    public double getBearing(Location l1, Location l2) {
        GeodesicData g = Geodesic.WGS84.Inverse(l1.lat, l1.lng, l2.lat, l2.lng);
        return g.azi2 > 0 ? g.azi2 : g.azi2 + 360;
        /*double dLon = Math.toRadians(l2.lng) - Math.toRadians(l1.lng);
         double l1lat = Math.toRadians(l1.lat);
         double l2lat = Math.toRadians(l2.lat);
         double y = Math.sin(dLon) * Math.cos(l2lat);
         double x = Math.cos(l1lat)*Math.sin(l2lat) -
         Math.sin(l1lat)*Math.cos(l2lat)*Math.cos(dLon);
         double brng = Math.atan2(y, x);
        
         return Math.toDegrees(brng);*/
    }

    public Image getStreetView(Location loc, double heading) {
        String url = "http://maps.googleapis.com/maps/api/streetview?"
                + "size=900x600&"
                + "location=" + loc + "&"
                + "fov=140&"
                + "heading=" + heading + "&"
                + "pitch=10&"
                + "sensor=false" + "&"
                + "key=" + API_KEY;
        try {
            return getHttpImage(url);
        } catch (MalformedURLException ex) {
            Logger.getLogger(StreetView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(StreetView.class.getName()).log(Level.SEVERE, null, ex);
        }


        return null;
    }

    public Image getHttpImage(String urlString) throws MalformedURLException, IOException {
        URL url = new URL(urlString);
        URLConnection con = url.openConnection();
        InputStream in = con.getInputStream();
        Image image = ImageIO.read(in);
        in.close();
        return image;
    }

    private String getHttpResponse(String urlString) throws MalformedURLException, IOException {
        URL url = new URL(urlString);
        URLConnection con = url.openConnection();
        con.addRequestProperty("Referer", "http://localhost");
        InputStream in = con.getInputStream();
        String encoding = con.getContentEncoding();
        encoding = (encoding == null) ? "UTF-8" : encoding;
        return slurp(in, encoding, 512);
    }

    private String slurp(final InputStream is, final String encoding, final int bufferSize) {
        final char[] buffer = new char[bufferSize];
        final StringBuilder out = new StringBuilder();
        try {
            final Reader in = new InputStreamReader(is, encoding);
            try {
                for (;;) {
                    int rsz = in.read(buffer, 0, buffer.length);
                    if (rsz < 0) {
                        break;
                    }
                    out.append(buffer, 0, rsz);
                }
            } finally {
                in.close();
            }
        } catch (UnsupportedEncodingException ex) {
            /* ... */
        } catch (IOException ex) {
            /* ... */
        }
        return out.toString();
    }

    /**
     * Distance in meters.
     *
     * @param l1
     * @param l2
     * @return
     */
    public double distance(Location l1, Location l2) {
        GeodesicData Inverse = Geodesic.WGS84.Inverse(l1.lat, l1.lng, l2.lat, l2.lng);
        return Inverse.s12;
    }

    public Location findPoint(Location l1, double dist) {
        Location l = new Location();
        GeodesicData gd = Geodesic.WGS84.Direct(l1.lat, l1.lng, l1.bearing, dist);
        l.lat = gd.lat2;
        l.lng = gd.lon2;
        l.bearing = gd.azi2 > 0 ? gd.azi2 : gd.azi2 + 360;
        System.out.println("l1.bearing = " + l1.bearing);
        System.out.println("gd.azi1 = " + gd.azi1 + "; gd.azi2 = " + gd.azi2);
        return l;
    }
}
