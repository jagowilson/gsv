/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package streetview.api;

import java.util.ArrayList;

/**
 *
 * @author Jeff
 */
public class RouteStep {
    public String direction = "null-direction";
    public ArrayList<Location> points = new ArrayList<>();
    public double distance = 0;
    
    @Override
    public String toString() {
        return direction;
    }
}
