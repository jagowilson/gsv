var ui;
var backend;

/**
 * Initialization function -- called on window load completion.
*/
var initialize = function() {
	backend = new Backend('#searchmap', '#seekmap');
	ui = new UI('#leftdirections', '#leftdirectionslist', '#searchbar', '#fromfield', '#tofield', '#searchmap', '#seekmap', '#videopane', '#video', '#streetview', backend);
	var dm = document.getElementById('dragmap');
	dm.addEventListener('dragstart',drag_start,false);
	document.body.addEventListener('dragover',drag_over,false);
	document.body.addEventListener('drop',drop,false); 
}

// Get it all started once the window loads.
google.maps.event.addDomListener(window, 'load', initialize);

/**
 * Called when the user clicks the "search" button on top.
 */
function search() {
	ui.search();
}

/**
 * Called when the user clicks "Get Directions"
 */
function showDirections() {
	ui.setMode(ui.modes.GET_DIRECTIONS);
}

/**
 * Called when the user clicks the "Get Directions" button.
 */
function directions() {
	ui.directions($('#fromfield').val(), $('#tofield').val());
}

/**
 * Called when the user clicks the play button.
 */
function play() {
	ui.play();
}

/**
 * Called when the user clicks the faster button.
 */
function faster(imageHandle) {
	ui.faster();
}

/**
 * Called when the user clicks the slower button.
 */
function slower(imageHandle) {
	ui.slower();
}

function drag_start(event) {
    var style = window.getComputedStyle(event.target, null);
    event.dataTransfer.setData("text/plain",
    (parseInt(style.getPropertyValue("left"),10) - event.clientX) + ',' + (parseInt(style.getPropertyValue("top"),10) - event.clientY));
}

function drop(event) {
    var offset = event.dataTransfer.getData("text/plain").split(',');
    var dm = document.getElementById('dragmap');
    dm.style.left = (event.clientX + parseInt(offset[0],10)) + 'px';
    dm.style.top = (event.clientY + parseInt(offset[1],10)) + 'px';
    event.preventDefault();
    return false;
}

function drag_over(event) {
    event.preventDefault();
    return false;
}