function Video(allSteps, stepIndex, canvas, seekMap, streetView, imageWidth, imageHeight, backend, ui, videoPlayingCallback, videoStoppedCallback) {
	this.allSteps = allSteps;
	this.stepIndex = stepIndex;
	this.routeStep = allSteps[stepIndex];
	this.canvas = $(canvas).get(0);
	this.imageWidth = imageWidth;
	this.imageHeight = imageHeight;
	this.seekMap = seekMap;
	this.fov = 120;
	this.backend = backend;
	this._cancelLoad = false;
	this.playing = false;
	this._loadIndex = 0;
	this._frameLoadIndex = 0;
	this._lastFrameLoadIndex = -1;
	this._playIndex = 0;
	this._loaded = false;
	this._polyline = this.backend.decodePolyline(this.routeStep.polyline.points);
	this._locations = new Array();
	this._overviewPolylines = new Array();
	this._panoIds = {};
	this._playTimeoutID = null;
	this._API_KEY = 'AIzaSyBwv6KcUySUpHdAYNeZCwPy30Xpr7hm8Bk';
	this._FASTEST_PLAY_MULT = 20.0; // MIN_PLAY_SPEED / 10 ms
	this._SLOWEST_PLAY_MULT = 1.0; // MIN_PLAY_SPEED / 1 ms
	this._MIN_PLAY_SPEED = 2000.0; // 1 second minimum speed
	this._PLAY_SPEED_INCREMENTS = 1;
	if (this.routeStep.distance.value < 100) {
		this._playSpeedMultiplier = 1.0; // 1x default speed
	} else {
		this._playSpeedMultiplier = 5.0; // 5x default speed
	}
	this._playSpeed = this._MIN_PLAY_SPEED / (this._playSpeedMultiplier * 2);
	this._videoPlayingCallback = videoPlayingCallback;
	this._videoStoppedCallback = videoStoppedCallback;
	this.streetView = streetView;
	this.ui = ui;
}

Video.prototype.showStreetView = function(f) {
	if (!f) {
		return;
	}
	
	$(this.canvas).hide();
	$(this.streetView).show();
	$(this.streetView).width(this.imageWidth);
	$(this.streetView).height(this.imageHeight);
	if (!this.ui.panorama) {
		this.ui._createPanorama();
	}
	this.ui.panorama.setPano(f.pano);
	this.ui.panorama.setPov({heading: f.heading, pitch: 0});
}

Video.prototype.showVideo = function() {
	$(this.canvas).show();
	$(this.streetView).hide();
}

Video.prototype.getNextFrame = function() {
	if (this._playIndex + 1 < this._locations.length) {
		return this._locations[this._playIndex + 1];
	}
	
	return null;
}

Video.prototype.getCurrentFrame = function() {
	if (this._playIndex < this._locations.length) {
		return this._locations[this._playIndex];
	} else if (this._playIndex >= this._locations.length) {
		return this._locations[this._locations.length - 1];
	} 	
	
	return null;
}

Video.prototype._computeOverviewPolylines = function() {
	var lineSymbol = {
		path: 'M 0,-0.1 0,0.1',
		strokeOpacity: 1,
		scale: 5
	};
	for (i = 0; i < this.allSteps.length; i++) {
		if (i != this.stepIndex) {
			var polySet = this.backend.decodePolyline(this.allSteps[i].polyline.points);
			var poly = new google.maps.Polyline({strokeColor: '#7984C9', strokeOpacity: 0, icons: [{
				icon: lineSymbol,
				offset: '0',
				repeat: '10px'
			}]});
			poly.setPath(polySet);
			this._overviewPolylines.push(poly);
		}
	}
}

Video.prototype._displayAllPolylines = function() {
	if (this._overviewPolylines.length == 0) {
		this._computeOverviewPolylines();
	}
	for (i = 0; i < this._overviewPolylines.length; i++) {
		this.backend.displaySeekMapPolyline(this._overviewPolylines[i]);
	}
	for (i = 0; i < this._locations.length; i++) {
		this.backend.displaySeekMapPolyline(this._locations[i].polyline);
	}
	this.backend.setSeekMapBounds(this._polyline);
}

Video.prototype.showPolyline = function() {
	if (!this._loaded) {
		return;
	}
	for (i = 0; i < this._overviewPolylines.length; i++) {
		this._overviewPolylines[i].setVisible(true);
	}
	for (i = 0; i < this._locations.length; i++) {
		this._locations[i].polyline.setVisible(true);
	}
}

Video.prototype.hidePolyline = function() {
	for (i = 0; i < this._overviewPolylines.length; i++) {
		this._overviewPolylines[i].setVisible(false);
	}
	for (i = 0; i < this._locations.length; i++) {
		this._locations[i].polyline.setVisible(false);
	}
}

Video.prototype._showNoVideoScreen = function() {
	var ctx = this.canvas.getContext('2d');
	ctx.fillStyle = "rgb(0,0,0)";
	ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
	ctx.fillStyle = "rgb(255,255,255)";
	ctx.strokeStyle = "rgb(255,255,255)";
	ctx.font = "20px Helvetica, Verdana, Arial, sans-serif";
	var textWidth = ctx.measureText("This step doesn't have any street view images so we couldn't make a video.").width;
	var x = (this.canvas.width - textWidth) / 2;
	var y = (this.canvas.height / 2) + 100;
	console.log(x + "; " + y);
	ctx.fillText("This step doesn't have any street view images so we couldn't make a video.", x, y);
} 

Video.prototype._showLoadingScreen = function(soFar, total) {
	this.canvas.style.top = '0px';
	var ctx = this.canvas.getContext('2d');
	ctx.fillStyle = "rgb(0,0,0)";
	ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
	ctx.fillStyle = "rgb(255,255,255)";
	ctx.strokeStyle = "rgb(255,255,255)";
	ctx.font = "20px Helvetica, Verdana, Arial, sans-serif";
	var textWidth = ctx.measureText("Please wait. Loading route data... 00%").width;
	var percent = Math.ceil(parseFloat(soFar) / parseFloat(total) * 100);
	var x = (this.canvas.width - textWidth) / 2;
	var y = (this.canvas.height / 2);
	var loadingY = y + (20 * 1.5);
	ctx.fillText("Please wait. Loading route data... " + percent + "%", x, y);
	ctx.strokeStyle = "rgb(0, 0, 255)";
	ctx.fillStyle = "rgb(0, 0, 255)";
	ctx.strokeRect(x, loadingY, textWidth, 40);
	ctx.fillRect(x, loadingY, textWidth * (parseFloat(soFar) / parseFloat(total)), 40);
}

Video.prototype._loadFrameData = function(index, lastIndex) {
	if (this._loaded) {
		return;
	}
	var idx = index ? index : this._frameLoadIndex;
	var lastIdx = typeof lastIndex != 'undefined' ? lastIndex : this._lastFrameLoadIndex;
	this._frameLoadIndex = idx;
	this._lastFrameLoadIndex = lastIdx;
	$(this.seekMap).hide();
	if (!this._cancelLoad) {
		var self = this;
		var latlng = this._polyline[idx];
		this._showLoadingScreen(idx, this._polyline.length);
		this.backend.requestPanoramaByLocation(latlng, function(results, status) {
			if (status == google.maps.StreetViewStatus.OK && !self._panoIds[results.location.pano]) {
				self._panoIds[results.location.pano] = true;
				var loc = self._makeLocation(latlng);
				loc.latlng = latlng;
				loc.pano = results.location.pano;
				self._locations.push(loc);
				if (self._locations.length >= 2) {
					loc.heading = self.backend.computeHeading(self._locations[self._locations.length - 2].latlng, self._locations[self._locations.length - 1].latlng);
					if (self._locations.length == 2) {
						self._locations[0].heading = self.backend.computeHeading(self._locations[0].latlng, self._locations[1].latlng);
					}
				}
				
				if (lastIndex != -1) {
					var polySet = self._polyline.slice(lastIndex, idx + 1);
					var poly = new google.maps.Polyline({strokeColor: '#666666'});
					poly.setPath(polySet);
					loc.polyline = poly;
				}
				if (idx == self._polyline.length - 1) {
					// Play it.
					self.canvas.style.top = '-328px'; // kind of a hack
					$(self.seekMap).show();
					self._displayAllPolylines();
					self._loaded = true;
					self.play();
				} else {
					self._loadFrameData(idx + 1, idx);
				}
			} else {
				if (idx == self._polyline.length - 1) {
					// Play it.
					self.canvas.style.top = '-328px'; // kind of a hack
					$(self.seekMap).show();
					self._displayAllPolylines();
					self._loaded = true;
					self.play();
				} else {
					self._loadFrameData(idx + 1, lastIndex);
				}
			}
		});
	}
}

Video.prototype.seekTo = function(seekIndex) {
	if (seekIndex >= this._locations.length) {
		return;
	}
	this._playIndex = seekIndex;
	this._loadIndex = seekIndex;
	this.stop();
}

Video.prototype.isPlaying = function() {
	return this.playing;
}

Video.prototype.play = function() {
	if (!this._loaded) {
		this._loadFrameData();
		return;
	}
	
	if (this._locations.length == 0) {
		this._showNoVideoScreen();
		return;
	}
	if (!this.playing) {
		this.showVideo();
		// if we made it to the end, go back to the beginning.
		if (this._playIndex >= this._locations.length) {
			this._playIndex = 0;
		}
		this.playing = true;
		this._videoPlayingCallback();
		this._drawFrame();
	}
}

Video.prototype.stop = function () {
	if (this.playing) {
		this.playing = false;
		if (this._playTimeoutID) {
			window.clearTimeout(this._playTimeoutID);
		}
		this._videoStoppedCallback();
	}
	var f = this.getCurrentFrame();
	this.showStreetView(f);
}

Video.prototype.faster = function() {
	if (this._playSpeedMultiplier < this._FASTEST_PLAY_MULT) {
		this._playSpeedMultiplier += this._PLAY_SPEED_INCREMENTS;
		this._playSpeed = this._MIN_PLAY_SPEED / (this._playSpeedMultiplier * 4);
	}
	
	return this._playSpeedMultiplier;
}

Video.prototype.slower = function() {
	if (this._playSpeedMultiplier > this._SLOWEST_PLAY_MULT) {
		this._playSpeedMultiplier -= this._PLAY_SPEED_INCREMENTS;
		this._playSpeed = this._MIN_PLAY_SPEED / (this._playSpeedMultiplier * 4);
	}
	
	return this._playSpeedMultiplier;
}

Video.prototype.getPlaySpeed = function() {
	return this._playSpeedMultiplier;
}

Video.prototype.forbidLoad = function() {
	this._cancelLoad = true;
}

Video.prototype.allowLoad = function() {
	this._cancelLoad = false;
}
Video.prototype.drawTemporaryFrame = function(locIndex) {
	this.showVideo();
	if (this._locations[locIndex].image) {
		this.canvas.getContext('2d').drawImage(this._locations[locIndex].image, 0, 0, this.imageWidth, this.imageHeight);
	}
}

Video.prototype._requestImage = function(index) {
	if (index >= this._locations.length) {
		return;
	}
	var self = this;
	var loc = this._locations[index];
	loc.requested = true;
	var i = new Image();
	i.src = this._getStreetViewImage(loc);
	i.addEventListener('load', function() {
		loc.image = this;
		var path = loc.polyline.getPath();
		loc.polyline.setOptions({strokeColor: '#0000FF'});
	}, false);
}

Video.prototype._drawFrame = function() {
	var self = this;
	if (this.playing) {
		var l = this._locations[this._playIndex];
		if (l) {
			if (l.image) {
				var ctx = this.canvas.getContext('2d');
				ctx.drawImage(l.image, 0, 0, this.imageWidth, this.imageHeight);
				this.backend.setSeekLocation(l.latlng, l.heading);
				this._playIndex += 1;
			} else if (l.requested) {
				// Load the next image.
				this._requestImage(this._loadIndex);
				this._loadIndex += 1;
			} else {
				// Load this image.
				this._requestImage(this._playIndex);
			}
		}
		if (this._playIndex < this._locations.length) {
			this._playTimeoutID = window.setTimeout(function() {
				self._drawFrame();
			}, this._playSpeed);
		} else {
			this.stop();
		}
	}
}

Video.prototype._getStreetViewImage = function(loc) {
	var scaledWidth = this.imageWidth <= 640 ? this.imageWidth : 640; // max 640
	var scaledHeight = Math.floor((640 * this.imageHeight)/this.imageWidth);
	var s = "http://maps.googleapis.com/maps/api/streetview?"
		+ "size=" + this.imageWidth + "x" + scaledHeight + "&"
		+ "pano=" + loc.pano+ "&"
		+ "fov=" + this.fov + "&"
		+ "heading=" + loc.heading + "&"
		+ "pitch=10&"
		+ "sensor=false" + "&"
		+ "key=" + this._API_KEY;
	return s;
}

Video.prototype._makeLocation = function(latlng) {
	var loc = {
		latlng: latlng,
		polyline: null,
		heading: 0,
		requested: false,
		image: null,
		pano: null
	};
	
	return loc;
}