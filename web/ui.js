function UI(directionsPane, directionsListPane, searchField, fromField, toField, searchMap, seekMap, videoPane, canvas, streetView, backend) {
	this.directionsPane = directionsPane;
	this.directionsListPane = directionsListPane;
	this.searchField = searchField;
	this.fromField = fromField;
	this.toField = toField;
	this.searchMap = searchMap;
	this.seekMap = seekMap;
	this.videoPane = videoPane;
	this.canvas = canvas;
	this.streetView = streetView;
	this.playButton = null;
	this.playControl = '#playcontrol';
	this.speedControl = '#speedcontrol';
	this.speedDisplay = '#speeddisplay';
	this.modes = {
		GET_DIRECTIONS: 0,
		SHOW_DIRECTIONS: 1,
		SHOW_STEP: 2
	};
	this.backend = backend;
	this.startLocation = null;
	this.endLocation = null;
	this.route = null;
	this.video = null;
	this.videos = new Array();
	this.contextMenu = null;
	this._videoPlaying = false;
	this._firstDrag = true;
	this._lastStepID = null;
	this._createPlayControlDiv();
	var self = this;
	this.backend.handlePolyClick = function(e) {
		if (self.getVideo()) {
			self.setMode(ui.modes.SHOW_STEP);
			var play = self.getVideo().isPlaying();
			self.getVideo().stop();
			var index = self.backend.findClosestPointOnPoly(e.latLng, self.getVideo()._locations);
			self.backend.setSeekLocation(self.getVideo()._locations[index].latlng, self.getVideo()._locations[index].heading);
			self.getVideo().seekTo(index);
			if (play) {
				self.getVideo().play();
			}
		}
	};
	this.backend.handleMarkerDrag = function() {
		if (self.getVideo()) {
			self.setMode(ui.modes.SHOW_STEP);
			if (self._firstDrag) {
				self._videoPlaying = self.getVideo().isPlaying();
				self._firstDrag = false;
				self.getVideo().stop();
				self.getVideo().streetViewListen = false;
			}
			var index = self.backend.findClosestPointOnPoly(ui.backend.seekMarker.getPosition(), ui.getVideo()._locations);
			self.backend.setSeekLocation(ui.getVideo()._locations[index].latlng, ui.getVideo()._locations[index].heading);
			self.getVideo().drawTemporaryFrame(index);
		}
	};
	this.backend.handleMarkerDragEnd = function() {
		if (self.getVideo()) {
			var index = self.backend.findClosestPointOnPoly(ui.backend.seekMarker.getPosition(), ui.getVideo()._locations);
			self.backend.setSeekLocation(ui.getVideo()._locations[index].latlng, ui.getVideo()._locations[index].heading);
			self.getVideo().seekTo(index);
			if (self._videoPlaying) {
				self.setMode(ui.modes.SHOW_STEP);
				self.getVideo().play();
			} else {
				self.getVideo().stop();
			}
			self._firstDrag = true;
		}
	};
	this.backend.registerRightClickHandler(function(event) {
		if (self.contextMenu) {
			self.contextMenu.style.display = 'none';
			self.contextMenu = null;
		}
		
		var menuOpts = {
			'Set as start point': function(el) {
					self.setStartFromEvent(event);
				},
			'Set as end point': function(el) {
					self.setEndFromEvent(event);
				}
		};
		var menuDiv = document.createElement('div');
		var list = document.createElement('ol');
		menuDiv.id = 'rightclickmenu';
		list.id = 'rightclickopt';
		for (var prop in menuOpts) {
			var li = document.createElement('li');
			li.innerHTML = prop;
			li.onclick = menuOpts[prop];
			list.appendChild(li);
		}
		menuDiv.appendChild(list);
		$(self.searchMap).get(0).appendChild(menuDiv);
		self.contextMenu = menuDiv;
		
		self.contextMenu.style.display = 'block';
		self.contextMenu.style.top = Math.floor(event.pixel.y) + "px";
		self.contextMenu.style.left = Math.floor(event.pixel.x) + "px";
	});
	this.backend.registerLeftClickHandler(function(event) {
		if (self.contextMenu) {
			self.contextMenu.style.display = 'none';
		}
	});
	this.backend.createPlayControl = function() {
		return self.playControlDiv;
	};
	this.setMode(this.modes.GET_DIRECTIONS);
}

UI.prototype._createPlayControlDiv = function() {
	var div = document.createElement('div');
	div.id = "playcontrol";
	this.playControl = '#playcontrol';
	var img = document.createElement('img');
	img.src = "play.png";
	img.alt = "play";
	img.onclick = play;
	img.style.cursor = 'pointer';
	img.id = "playbutton";
	div.appendChild(img);
	this.playButton = img;
	var span = document.createElement('div');
	span.id = 'speedcontrol';
	var fasterImg = document.createElement('img');
	var slowerImg = document.createElement('img');
	fasterImg.src = "faster.png";
	fasterImg.alt = "faster";
	fasterImg.onclick = faster;
	fasterImg.style.cursor = 'pointer';
	fasterImg.className = 'speedbutton';
	slowerImg.src = "slower.png";
	slowerImg.alt = "slower";
	slowerImg.onclick = slower;
	slowerImg.className = 'speedbutton';
	slowerImg.style.cursor = 'pointer';
	var text = document.createElement("p");
	this.speedText = text;
	text.id = "speeddisplay";
	this.speedDisplay = '#speeddisplay';
	span.appendChild(fasterImg);
	span.appendChild(text);
	span.appendChild(slowerImg);
	this.speedControl = '#speedcontrol';
	div.appendChild(span);
	this.playControlDiv = div;
}

UI.prototype._createPanorama = function() { 
	var w = $(this.canvas).get(0).width;
	var h = $(this.canvas).get(0).height;
	$(this.streetView).width(w);
	$(this.streetView).height(h);
	this.panorama = this.backend.createStreetView($(this.streetView).get(0), new google.maps.LatLng(35.779580, -78.638200), 0);
	var self = this;
	google.maps.event.addListener(this.panorama, 'pano_changed', function() {
		self.backend.setSeekLocation(self.panorama.getPosition(), self.panorama.getPov().heading);
	});

	google.maps.event.addListener(this.panorama, 'position_changed', function() {
		self.backend.setSeekLocation(self.panorama.getPosition(), self.panorama.getPov().heading);
	});

	google.maps.event.addListener(this.panorama, 'pov_changed', function() {
		self.backend.setSeekLocation(self.panorama.getPosition(), self.panorama.getPov().heading);
	});
}

UI.prototype.setStartFromEvent = function(event) {
	this.hideContextMenu();
	this.setFrom(event.latLng.ob + "," + event.latLng.pb);
	this.backend.setOriginLocationMarker(event.latLng);
}

UI.prototype.setEndFromEvent = function(event) {
	this.hideContextMenu();
	this.setTo(event.latLng.ob + "," + event.latLng.pb);
	this.backend.setDestinationLocationMarker(event.latLng);
}

UI.prototype.hideContextMenu = function() {
	this.contextMenu.style.display = 'none';
}

UI.prototype.play = function() {
	if (this.video) {
		if (this.video.playing) {
			this.video.stop();
			this.showPlayButton();
		} else {
			this.video.play();
			$(this.playButton).attr("src", "pause.png");
			$(this.playButton).attr("alt", "pause");
		}
	}
}

UI.prototype.showPlayButton = function() {
	this.playButton.src = "play.png";
	this.playButton.alt = "play";
	$(this.speedDisplay).text(this.video.getPlaySpeed() + "x");
}

UI.prototype.showPauseButton = function() {
	this.playButton.src = "pause.png";
	this.playButton.alt = "pause";
	$(this.speedDisplay).text(this.video.getPlaySpeed() + "x");
}


UI.prototype.faster = function() {
	if (this.video) {
		var v = this.video.faster();
		$(this.speedDisplay).text(v + "x");
	}
}

UI.prototype.slower = function() {
	if (this.video) {
		var v = this.video.slower();
		$(this.speedDisplay).text(v + "x"); 	
	}
}

/**
 * Returns the currently active video.
 */
UI.prototype.getVideo = function() {
	return this.video;
}

/**
 * Get the value of the "from" field.
 */
UI.prototype.getFrom = function() {
	return $(this.fromField).val();
}

/**
 * Get the value of the "to" field.
 */
UI.prototype.getTo = function() {
	return $(this.toField).val();
}

/**
 * Set the value of the "from" field in the directions box.
 */
UI.prototype.setFrom = function(from) {
	$(this.fromField).val(from);
};

/**
 * Set the value of the "to" field in the directions box.
 */
UI.prototype.setTo = function(to) {
	$(this.toField).val(to);
};

/**
 * Set the mode of the application. This determines what is displayed in the left pane.
 */
UI.prototype.setMode = function (newMode) {
	this.mode = newMode;
	$(this.directionsPane).hide();
	$(this.directionsListPane).hide();
	$(this.locationResultPane).hide();
	$(this.searchMap).hide();
	$(this.seekMap).hide();
	$(this.videoPane).hide();
	$(this.canvas).hide();
	$(this.streetView).hide();
	
	if (newMode == this.modes.GET_DIRECTIONS) {
		$(this.directionsPane).show();
		$(this.searchMap).show();
		this.videos = new Array();
	} else if (newMode == this.modes.SHOW_DIRECTIONS) {
		$(this.directionsPane).show();
		$(this.directionsListPane).show();
		$(this.searchMap).show();
		this.backend.setSingleLocationMarker(null);
	} else if (newMode == this.modes.SHOW_STEP) {
		$(this.directionsPane).show();
		$(this.directionsListPane).show();
		$(this.videoPane).show();
		$(this.seekMap).show();
		$(this.canvas).show();
	} else {
		return;
	}
};

/**
 * Show a particular location on the map, from a search query.
 
 * Invoked when the user clicks the search button or presses enter on the search bar.
*/
UI.prototype.search = function() {
	var self = this;
	// Is this one location, or can it be divided into two parts by the keyword 'to'?
	var searchQuery = $(this.searchField).val().trim();
	if (searchQuery.length == 0) {
		// Empty string.
		return;
	}
	var splitQuery = searchQuery.split(' to ', 2);
	
	if (splitQuery.length == 1) {
		this.backend.searchForLocation(searchQuery, function(results, status) {
			if (results.length == 0) {
				return;
			}
			var location = results[0];
			self.setMode(self.modes.GET_DIRECTIONS);
			self.setTo(location.formatted_address);
			self.backend.setSearchMapCenter(location.geometry.location);
			self.backend.setSearchMapZoom(15);
			self.backend.setSingleLocationMarker(location.geometry.location);
		});
	} else if (splitQuery.length == 2) {
		var fromLocation = splitQuery[0].trim();
		var toLocation = splitQuery[1].trim();
		this.directions(fromLocation, toLocation);
	}
};

/**
 * Get directions for a from,to pair entered in the directions box on the left.
 
 * Invoked when the user clicks "Get Directions"
 */
UI.prototype.directions = function(from, to) {
	var self = this;
	this.setMode(this.modes.GET_DIRECTIONS);
	this.setFrom(from);
	this.setTo(to);
	this.from = from;
	this.to = to;
	this.backend.searchForLocation(from, function(results, status) {
		var location = results[0];
		self.startLocation = location;
		self.backend.searchForLocation(ui.to, function(results, status) {
			var location = results[0];
			self.endLocation = location;
			self.setMode(ui.modes.SHOW_DIRECTIONS);
			self.backend.requestDirections(ui.startLocation, ui.endLocation, function(results, status) {
				var route = results.routes[0]; // TODO: Multiple selections?
				self._populateDirectionsList(route);
			});
		});
	});
};

/**
 * Populates the directions list.
 */
UI.prototype._populateDirectionsList = function(route) {
	this._lastStepID = null;
	// Install map markers.
	this.backend.setOriginLocationMarker(this.startLocation.geometry.location);
	this.backend.setDestinationLocationMarker(this.endLocation.geometry.location);
	this.backend.setSearchMapZoom(15);
	this.backend.setSearchMapBounds(route.bounds);
	this.backend.displaySearchMapPolyline(route.overview_polyline.points);
	this.route = route;
	var newText = '<p id="mapoverview" class="overviewclicked">Route Overview</p>';
	newText += '<p id="clickon">Click on a direction to start driving.</p>';
	newText += '<ol id="clickabledirections">';
	for (i = 0; i < route.legs[0].steps.length; i++) {
		var step = route.legs[0].steps[i];
		newText += '<li class="clickdirection" id="step' + i + '"><span class="distance">' + step.distance.text + '; ' + step.duration.text + '</span><span class="direction">' + step.instructions + '</span></li>';
	}
	newText += '</ol>';
	$('#leftdirectionslist').html(newText);
	this._installDirectionsClicks();
	this.videos.length = route.legs[0].steps.length;
};

UI.prototype._showStep = function(stepIndex) {
	var self = this;
	var step = this.route.legs[0].steps[stepIndex];
	
	if (this.video) {
		this.video.forbidLoad();
		this.video.stop();
		this.video.hidePolyline();
	}
	
	this.setMode(this.modes.SHOW_STEP);
	this.backend.createSeekMap();
	var canvasWidth = $(this.videoPane).outerWidth(true);
	var canvasHeight = $(this.videoPane).outerHeight(true);
	$(this.canvas).get(0).width = canvasWidth;
	$(this.canvas).get(0).height = canvasHeight;
	$(this.streetView).get(0).width = canvasWidth;
	$(this.streetView).get(0).height = canvasHeight;
	if (this.videos[stepIndex]) {
		this.video = this.videos[stepIndex];
		if (this.speedText) {
			this.speedText.innerHTML = this.video.getPlaySpeed() + "x";
		}
		this.video.allowLoad();
		this.video.showPolyline();
		this.video.play();
	} else {
		this.video = new Video(this.route.legs[0].steps, stepIndex, this.canvas, '#dragmap', this.streetView, canvasWidth, canvasHeight, this.backend, this, function() {
			self.showPauseButton();
		}, function() {
			self.showPlayButton();
		});
		this.video.play();
		this.videos[stepIndex] = this.video;
	}
}

/**
 * Sets up the directions list so it is clickable.
 *
 * Must be called anytime the list is re-populated.
 */
UI.prototype._installDirectionsClicks = function() {
	var self = this;
	$('#mapoverview').click(function() {
		self.setMode(self.modes.SHOW_DIRECTIONS);
			if (self._lastStepID) {
				$('#' + self._lastStepID).toggleClass('activestep', false);
				$('#' + self._lastStepID).toggleClass('clickdirection', true);
			}
			$('#mapoverview').toggleClass('overviewclick', false);
			$('#mapoverview').toggleClass('overviewclicked', true);
			self._lastStepID = null;
	});
	$('.clickdirection').click(function() {
		$('#mapoverview').toggleClass('overviewclicked', false);
		$('#mapoverview').toggleClass('overviewclick', true);
		if (self._lastStepID) {
			if (self._lastStepID == this.id) {
				return;
			}
			$('#' + self._lastStepID).toggleClass('activestep', false);
			$('#' + self._lastStepID).toggleClass('clickdirection', true);
		}
		self.setMode(self.modes.SHOW_STEP);
		// The ID starts with the word "step", so remove it.
		self._showStep(parseInt(this.id.substring(4, this.id.length)));
		self._lastStepID = this.id;
		$('#' + this.id).toggleClass('activestep', true);
		$('#' + this.id).toggleClass('clickdirection', false);
	});
};