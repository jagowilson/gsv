function Backend(searchMapId, seekMapId) {
	this.searchMapEl = $(searchMapId).get(0);
	this.seekMapEl = $(seekMapId).get(0);
	this.createPlayControl = null;
	this.seekMap = null;
	this.searchMapPoly = null;
	this.loadedPoly = null;
	this.unloadedPoly = null;
	this.seekMapPoly = null;
	this.searchMap = this._initializeMap(this.searchMapEl);
	this.geocoder = new google.maps.Geocoder;
	this.streetview = new google.maps.StreetViewService;
	this.directionsService = new google.maps.DirectionsService;
	this.singleLocationMarker = null;
	this.originLocationMarker = null;
	this.destinationLocationMarker = null;
	this.seekMarker = null;
	this.draggingMarker = false; // if the seek marker is being dragged
	this.handleMarkerDrag = null;
	this.handleMarkerDragEnd = null;
	this.handlePolyClick = null;
	this.rightClickHandler = null;
}

Backend.prototype.registerLeftClickHandler = function(handler) {
	google.maps.event.addListener(this.searchMap, "click", handler);
}

Backend.prototype.registerRightClickHandler = function(handler) {
	google.maps.event.addListener(this.searchMap, "rightclick", handler);
	this.rightClickHandler = handler;
}

/**
 * Requests directions from the Google API.
 */
Backend.prototype.requestDirections = function(origin, destination, callback) {
	var request = {
		origin: origin.geometry.location,
		destination: destination.geometry.location,
		travelMode: google.maps.TravelMode.DRIVING
	};
	this.directionsService.route(request, callback);
};

/**
 * Requests information about a particular panorama from the Street View service.
 */
Backend.prototype.requestPanoramaByLocation = function(latlng, callback) {
	this.streetview.getPanoramaByLocation(latlng, 10, callback);
}

Backend.prototype.computeHeading = function(from, to) {
	return google.maps.geometry.spherical.computeHeading(from, to);
}

Backend.prototype.computeDistance = function(from, to) {
	return google.maps.geometry.spherical.computeDistanceBetween(from, to);
}

/**
 * Geodecodes any given location and calls the given callback.
*/
Backend.prototype.searchForLocation = function(searchQuery, callback) {
	// Decode it using the Google API.
	var re = new RegExp("^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$");
	if (re.test(searchQuery)) {
		var s = searchQuery.split(",");
		var lat = parseFloat(s[0]);
		var lon = parseFloat(s[1]);
		var request = {
			latLng: new google.maps.LatLng(lat, lon) 
		};
		this.geocoder.geocode(request, callback);
	} else {
		var request = {
			address: searchQuery
		};
		this.geocoder.geocode(request, callback);
	}
};

/**
 * Set the center of the search map.
 */
Backend.prototype.setSearchMapCenter = function(latlng) {
	this.searchMap.setCenter(latlng);
};

/**
 * Set the center of the search map.
 */
Backend.prototype.setSeekMapCenter = function(latlng) {
	if (this.seekMap) {
		this.seekMap.setCenter(latlng);
	}
};

/**
 * Set the zoom of the search map.
 */
Backend.prototype.setSearchMapZoom = function(zoom) {
	this.searchMap.setZoom(zoom);
};

/**
 * Set a single location marker on the search map.
 */
Backend.prototype.setSingleLocationMarker = function(latlng) {
	var markerOptions = {
		position: latlng,
		map: this.searchMap
	};
	if (this.singleLocationMarker) {
		this.singleLocationMarker.setMap(null);
		this.singleLocationMarker = null;
	}
	if (latlng) {
		this.singleLocationMarker = new google.maps.Marker(markerOptions);
	}
};

/**
 * Set the origin location marker on the search map.
 */
Backend.prototype.setOriginLocationMarker = function(latlng) {
	var markerOptions = {
		position: latlng,
		map: this.searchMap,
		icon: 'darkgreen_MarkerA.png'
	};
	if (this.originLocationMarker) {
		this.originLocationMarker.setMap(null);
		this.originLocationMarker = null;
	}
	this.originLocationMarker = new google.maps.Marker(markerOptions);
}

/**
 * Set the destination location marker on the search map.
 */
Backend.prototype.setDestinationLocationMarker = function(latlng) {
	var markerOptions = {
		position: latlng,
		map: this.searchMap,
		icon: 'red_MarkerB.png'
	};
	if (this.destinationLocationMarker) {
		this.destinationLocationMarker.setMap(null);
		this.destinationLocationMarker = null;
	}
	this.destinationLocationMarker = new google.maps.Marker(markerOptions);
};

/**
 * Set the bounds of the search map.
 */
Backend.prototype.setSearchMapBounds = function(bounds) {
	this.searchMap.fitBounds(bounds);
};

Backend.prototype.decodePolyline = function(polyline) {
	return google.maps.geometry.encoding.decodePath(polyline);
}
/**
 * Displays a poly line on the search map.
 */
Backend.prototype.displaySearchMapPolyline = function(polyline) {
	if (this.searchMapPoly) {
		this.searchMapPoly.setMap(null);
		this.searchMapPoly = null;
	}
	var decodedSets = this.decodePolyline(polyline);
	this.searchMapPoly = new google.maps.Polyline({strokeColor: '#0000FF'});
	this.searchMapPoly.setPath(decodedSets);
	this.searchMapPoly.setMap(this.searchMap);
};

/**
 * Displays a poly line on the search map.
 */
Backend.prototype.displaySeekMapPolyline = function(polyline) {
	if (!this.seekMap) {
		return;
	}
	google.maps.event.addDomListener(polyline, 'click', this.handlePolyClick);
	polyline.setOptions({strokeWeight: 5});
	polyline.setMap(this.seekMap);
};

Backend.prototype.setSeekMapBounds = function(polyline) {
	if (!this.seekMap) {
		return;
	}
	// Set the bounds of the map.
	var bounds = new google.maps.LatLngBounds();
	for (var i = 0; i < polyline.length; i++) {
		bounds.extend(polyline[i]);
	}
	this.seekMap.fitBounds(bounds);
	this.setSeekLocation(null);
}

Backend.prototype.setSeekLocation = function(latlng, rotation) {
	if (!this.seekMap) {
		return;
	}
	 
	if (latlng && !this.seekMap.getBounds().contains(latlng)) {
		this.seekMap.panTo(latlng);
	}
	var rotation = rotation || 0;
	var icon =  {
					path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
					scale: 4,
					rotation: rotation
				};
	if (!this.seekMarker) {
		var markerOptions = {
			position: latlng,
			map: this.seekMap,
			icon: icon,
			draggable: true,
			crossOnDrag: false
		};
		this.seekMarker = new google.maps.Marker(markerOptions);
		google.maps.event.addDomListener(this.seekMarker, 'dragend', this.handleMarkerDragEnd);
		google.maps.event.addDomListener(this.seekMarker, 'drag', this.handleMarkerDrag);
	} else {
		if (latlng) {
			if (!this.seekMarker.getMap()) {
				this.seekMarker.setMap(this.seekMap);
			}
			this.seekMarker.setPosition(latlng);
			this.seekMarker.setIcon(icon);
		} else {
			this.seekMarker.setMap(null);
		}
	}
}

Backend.prototype.findClosestPointOnPoly = function(latlng, polyline) {
	var lowestIndex = 0;
	var lowestValue = Number.MAX_VALUE;
	
	for (i = 0; i < polyline.length; i++) {
		var dist = this.computeDistance(polyline[i].latlng, latlng);
		if (dist < lowestValue) {
			lowestIndex = i;
			lowestValue = dist;
		}
	}
	
	return lowestIndex;
}

/**
 * Creates a new instance of the seek map. Must be called after the div containing the seek map
 * has been made visible.
 */
Backend.prototype.createSeekMap = function() {
	this.seekMapPoly = null;
	
	if (!this.seekMap) {
		var mapOptions = {
		  center: new google.maps.LatLng(35.779580, -78.638200),
		  zoom: 15,
		  mapTypeControl: true,
		  mapTypeControlOptions: {
				style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
				position: google.maps.ControlPosition.RIGHT_BOTTOM
			},

		  zoomControl: false,
		  streetViewControl: false,
		};
		this.seekMap = this._initializeMap(this.seekMapEl, mapOptions);
		var playDiv = this.createPlayControl();
		this.seekMap.controls[google.maps.ControlPosition.TOP_LEFT].push(playDiv);
	}
}

Backend.prototype.createStreetView = function(element, position, heading) {
	var panoramaOptions = {
		position: position,
		pov: {
			heading: heading,
			pitch: 10
		},
		disableDefaultUI: true,
		linksControl: true
	};
	var self = this;
	var panorama = new google.maps.StreetViewPanorama(element, panoramaOptions);

	return panorama;
}

/**
 * Initializes the map.
 */
Backend.prototype._initializeMap = function(mapEl, mapOptions) {
	var opts = mapOptions || {
	  center: new google.maps.LatLng(35.779580, -78.638200),
	  zoom: 8,
	};
	var map = new google.maps.Map(mapEl, opts);
	return map;
};